package com.gg.gg;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
public class GGGUI extends UI {

    private DialogService dialogService;
    private HistoryService historyService;

    private TextArea textArea = new TextArea("");

    @Autowired
    public GGGUI(DialogService dialogService, HistoryService historyService) {
        this.dialogService = dialogService;
        this.historyService = historyService;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        VerticalLayout components = new VerticalLayout();
        Label label = new Label("gg messenger");
        TextArea textArea = new TextArea("");
        TextField textField = new TextField("");
        HorizontalLayout horizontalLayout = new HorizontalLayout();


        Button button = new Button("Send");


        button.addClickListener(clickEvent -> {
            String reso = dialogService.sentMessage(textField.getValue());
            historyService.addHistory(textField.getValue());

            textArea.setValue(historyService.getHistory());
        });

        components.addComponent(label);
        components.addComponent(textArea);
        components.addComponent(textField);

        horizontalLayout.addComponent(button);

        components.addComponent(horizontalLayout);
        setContent(components);

        UI ui = UI.getCurrent();
        ui.setPollInterval(1000);
        ui.addPollListener(x -> {
            textArea.setValue(historyService.getHistory());
        });
    }


}