package com.gg.gg;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DialogService {


    public String sentMessage(String infoToSent) {
        RestTemplate rest = new RestTemplate();
        HttpEntity<String> rssResponse = rest.exchange(
                "http://localhost:8080/get?text=" + infoToSent,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class);
        return rssResponse.getBody();
    }
}