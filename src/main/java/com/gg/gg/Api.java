package com.gg.gg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Api {

    @Autowired
    private HistoryService historyService;

    @RequestMapping("/get")
    @ResponseBody
    public void sentText(@RequestParam("text") String text) {
        historyService.addHistory(text);
    }
}